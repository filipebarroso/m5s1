import 'package:app_francesinha/public/features/application/navigation_extensions.dart';
import 'package:app_francesinha/shared/features/authentication/presentation/authentication_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AuthenticationPage extends ConsumerWidget {
  const AuthenticationPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final controller = ref.watch(authenticationPageControllerProvider.notifier);
    final state = ref.watch(authenticationPageControllerProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Authentication'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                context.navigateEmailLoginPage();
              },
              child: const Text('Email Login'),
            ),
            const SizedBox(height: 18),
            ElevatedButton(
              onPressed: () {
                context.navigateEmailRegisterPage();
              },
              child: const Text('Email Register'),
            ),
          ],
        ),
      ),
    );
  }
}
