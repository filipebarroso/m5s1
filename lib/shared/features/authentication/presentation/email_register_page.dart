import 'package:app_francesinha/shared/features/authentication/presentation/email_register_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class EmailRegisterPage extends ConsumerStatefulWidget {
  const EmailRegisterPage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _EmailRegisterPageState();
}

class _EmailRegisterPageState extends ConsumerState<EmailRegisterPage> {
  late final TextEditingController _nameController;
  late final TextEditingController _emailController;
  late final TextEditingController _passwordController;

  @override
  void initState() {
    super.initState();

    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final controller = ref.watch(emailRegisterPageControllerProvider.notifier);
    final state = ref.watch(emailRegisterPageControllerProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Email Register'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            /// Nome
            const SizedBox(height: 18),
            const Text('Nome'),
            TextField(
              controller: _nameController,
              keyboardType: TextInputType.name,
            ),

            /// Email
            const SizedBox(height: 18),
            const Text('Email'),
            TextField(
              controller: _emailController,
              keyboardType: TextInputType.emailAddress,
            ),

            /// Password
            const SizedBox(height: 18),
            const Text('Password'),
            TextField(
              controller: _passwordController,
              obscureText: true,
            ),

            if (state.isLoading)
              const Center(child: CircularProgressIndicator()),

            /// SUBMIT
            const SizedBox(height: 18),
            ElevatedButton(
              onPressed: () {
                _submit(controller);
                //
              },
              child: const Text('Submit'),
            ),
          ],
        ),
      ),
    );
  }

  void _submit(EmailRegisterPageController controller) {
    final name = _nameController.text;
    final email = _emailController.text;
    final password = _passwordController.text;

    if (name.isEmpty || email.isEmpty || password.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Preencha todos os campos'),
        ),
      );
    } else {
      controller.register(name: name, email: email, password: password);
    }
  }
}
