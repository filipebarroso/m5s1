import 'package:app_francesinha/shared/features/authentication/application/authentication_base.dart';
import 'package:app_francesinha/shared/features/authentication/application/authentication_firebase_service.dart';
import 'package:app_francesinha/shared/features/authentication/data/authentication_repository.dart';
import 'package:app_francesinha/shared/features/authentication/domain/user_entity.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class EmailRegisterPageControllerState {
  const EmailRegisterPageControllerState(this.isLoading, this.user);

  final bool isLoading;
  final UserEntity? user;

  EmailRegisterPageControllerState copyWith({
    bool? isLoading,
    UserEntity? user,
  }) {
    return EmailRegisterPageControllerState(
      isLoading ?? this.isLoading,
      user ?? this.user,
    );
  }
}

class EmailRegisterPageController
    extends StateNotifier<EmailRegisterPageControllerState> {
  EmailRegisterPageController(AuthenticationBase authenticationService)
      : super(
          const EmailRegisterPageControllerState(false, null),
        ) {
    _authenticationService = AuthenticationRepository(authenticationService);
  }

  late final AuthenticationRepository _authenticationService;

  Future<void> register({
    required String name,
    required String email,
    required String password,
  }) async {
    state = state.copyWith(isLoading: true);

    final user = _authenticationService.createUserWithEmailAndPassword(
      email: email,
      password: password,
      displayName: name,
    );

    state = state.copyWith(isLoading: false, user: user as UserEntity);
  }
}

final emailRegisterPageControllerProvider = StateNotifierProvider.autoDispose<
    EmailRegisterPageController, EmailRegisterPageControllerState>(
  (ref) {
    // Here we call the [AuthenticationFirebaseService] but we could call the [AuthenticationOtherService] instead.
    final authService = ref.watch(authenticationFirebaseServiceProvider);

    return EmailRegisterPageController(authService);
  },
);
