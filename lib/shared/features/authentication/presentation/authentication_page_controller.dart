import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

@immutable
class AuthenticationPageControllerState {
  const AuthenticationPageControllerState(this.isLoading);

  final bool isLoading;

  AuthenticationPageControllerState copyWith({
    bool? isLoading,
  }) {
    return AuthenticationPageControllerState(
      isLoading ?? this.isLoading,
    );
  }
}

class AuthenticationPageController
    extends StateNotifier<AuthenticationPageControllerState> {
  AuthenticationPageController()
      : super(const AuthenticationPageControllerState(false));

  void login() {
    // state = state.copyWith(isLoading: true);
    // QLQ coisa
    // state = state.copyWith(isLoading: false);
  }

  void register() {
    // state = state.copyWith(isLoading: true);
  }
}

final authenticationPageControllerProvider = StateNotifierProvider<
    AuthenticationPageController, AuthenticationPageControllerState>(
  (ref) => AuthenticationPageController(),
);
