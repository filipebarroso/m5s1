import 'package:app_francesinha/shared/features/authentication/application/authentication_base.dart';

class AuthenticationRepository {
  final AuthenticationBase _authenticationBase;

  AuthenticationRepository(this._authenticationBase);

  void createUserWithEmailAndPassword({
    required String email,
    required String password,
    required String displayName,
  }) {
    _authenticationBase.createUserWithEmailAndPassword(
      email: email,
      password: password,
      displayName: displayName,
    );

    // Edit the State
  }

  void signInWithEmailAndPassword({
    required String email,
    required String password,
  }) {
    _authenticationBase.signInWithEmailAndPassword(
      email: email,
      password: password,
    );

    // Edit the State
  }
}
