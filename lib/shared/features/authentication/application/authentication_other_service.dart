import 'package:app_francesinha/shared/features/authentication/application/authentication_base.dart';
import 'package:app_francesinha/shared/features/authentication/domain/user_entity.dart';

class AuthenticationOtherService implements AuthenticationBase {
  @override
  Future<UserEntity> createUserWithEmailAndPassword(
      {required String email,
      required String password,
      required String displayName}) {
    // TODO: implement createUserWithEmailAndPassword
    throw UnimplementedError();
  }

  @override
  Future<UserEntity> signInWithEmailAndPassword(
      {required String email, required String password}) {
    // TODO: implement signInWithEmailAndPassword
    throw UnimplementedError();
  }
}
