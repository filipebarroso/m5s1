// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_firebase_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authenticationFirebaseServiceHash() =>
    r'bc07caae37a1811df4f2b9d8e2efc5d7970e0f2c';

/// See also [AuthenticationFirebaseService].
@ProviderFor(AuthenticationFirebaseService)
final authenticationFirebaseServiceProvider = NotifierProvider<
    AuthenticationFirebaseService, AuthenticationFirebaseService>.internal(
  AuthenticationFirebaseService.new,
  name: r'authenticationFirebaseServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$authenticationFirebaseServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AuthenticationFirebaseService
    = Notifier<AuthenticationFirebaseService>;
// ignore_for_file: unnecessary_raw_strings, subtype_of_sealed_class, invalid_use_of_internal_member, do_not_use_environment, prefer_const_constructors, public_member_api_docs, avoid_private_typedef_functions
