import 'package:app_francesinha/shared/features/authentication/domain/user_entity.dart';

abstract class AuthenticationBase {
  Future<UserEntity> createUserWithEmailAndPassword({
    required String email,
    required String password,
    required String displayName,
  });

  Future<UserEntity> signInWithEmailAndPassword({
    required String email,
    required String password,
  });
}
