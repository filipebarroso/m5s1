import 'package:app_francesinha/shared/features/authentication/application/authentication_base.dart';
import 'package:app_francesinha/shared/features/authentication/domain/user_entity.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:firebase_auth/firebase_auth.dart';

part 'authentication_firebase_service.g.dart';

@Riverpod(keepAlive: true)
class AuthenticationFirebaseService extends _$AuthenticationFirebaseService
    implements AuthenticationBase {
  late FirebaseAuth _firebaseAuth;

  @override
  AuthenticationFirebaseService build() {
    _firebaseAuth = FirebaseAuth.instance;
    return AuthenticationFirebaseService();
  }

  @override
  Future<UserEntity> createUserWithEmailAndPassword({
    required String email,
    required String password,
    String? displayName,
  }) async {
    try {
      final credential = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      if (displayName is String && displayName.isNotEmpty) {
        await credential.user?.updateDisplayName(displayName);
      }

      final UserEntity userEntity = UserEntity<UserCredential>(credential);
      return userEntity;
    } catch (err) {
      if (err is FirebaseAuthException) {
        throw Exception(err.message);
      }
    }

    throw Exception('Error creating user');
  }

  @override
  Future<UserEntity> signInWithEmailAndPassword(
      {required String email, required String password}) {
    // TODO: implement signInWithEmailAndPassword
    throw UnimplementedError();
  }
}
