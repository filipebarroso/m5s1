import 'package:app_francesinha/public/features/detail/application/service_base.dart';
import 'package:app_francesinha/public/features/detail/domain/francesinha.dart';

// Repository is where the data "lives", it leverages the Services to grab the
// data and return it depending on the use case.
// The page Controller will use this repository to get the data.
// Its here in the repository also that we handle any other feature for handling data such as parsing data, persistence, authentication etc.
class ServiceRepository {
  final ServiceBase _serviceBase;

  // This can be [ServiceHttp] or [ServiceDio]
  ServiceRepository(this._serviceBase);

  Future<FoodItemEntity> getFrancesinha(int id) async {
    return _serviceBase.getFrancesinha(id);
  }

  Future<List<FoodItemEntity>> getFrancesinhas() async {
    return _serviceBase.getFrancesinhas();
  }
}
