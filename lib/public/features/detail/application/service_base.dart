import 'package:app_francesinha/public/features/detail/domain/francesinha.dart';

abstract class ServiceBase {
  Future<FoodItemEntity> getFrancesinha(int id);
  Future<List<FoodItemEntity>> getFrancesinhas();
}
