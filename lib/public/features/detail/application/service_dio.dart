import 'package:app_francesinha/public/features/detail/application/service_base.dart';
import 'package:app_francesinha/public/features/detail/domain/francesinha.dart';
import 'package:app_francesinha/shared/constants.dart';
import 'package:dio/dio.dart';

class ServiceDio implements ServiceBase {
  late final Dio _dio;
  ServiceDio() {
    _dio = Dio();
  }

  @override
  Future<FoodItemEntity> getFrancesinha(int id) async {
    final response = await _dio.get('$host/francesinha/$id');
    return FoodItemEntity.fromRawJson(response.data);
  }

  @override
  Future<List<FoodItemEntity>> getFrancesinhas() async {
    final response = await _dio.get('$host/francesinha');
    return francesinhasFromJson(response.data);
  }
}
