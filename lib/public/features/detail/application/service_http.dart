import 'package:app_francesinha/public/features/detail/application/service_base.dart';
import 'package:app_francesinha/public/features/detail/domain/francesinha.dart';
import 'package:app_francesinha/shared/constants.dart';
import 'package:http/http.dart';

class ServiceHttp implements ServiceBase {
  late final Client _client;

  ServiceHttp() {
    _client = Client();
  }

  @override
  Future<FoodItemEntity> getFrancesinha(int id) async {
    final uri = Uri.https(host, 'francesinha/$id');
    final response = await _client.get(uri);
    return FoodItemEntity.fromRawJson(response.body);
  }

  @override
  Future<List<FoodItemEntity>> getFrancesinhas() async {
    final uri = Uri.https(host, 'francesinha');
    final response = await _client.get(uri);
    return francesinhasFromJson(response.body);
  }

  void close() {
    _client.close();
  }
}
