import 'package:app_francesinha/public/features/cart/application/bloc/cart_bloc.dart';
import 'package:app_francesinha/public/features/cart/application/riverpod/cart_provider.dart';
import 'package:app_francesinha/public/features/home/presentation/widgets/list_menu_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CartPageView extends ConsumerWidget {
  const CartPageView({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final cart = ref.watch(cartProvider);

    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        final selectedFood = state.items;
        if (cart.isEmpty) {
          return const Center(
            child: Text('No items in cart'),
          );
        } else {
          return Column(
            children: [
              ElevatedButton(
                onPressed: () {
                  /// Clear cart
                  // context.read<CartBloc>().add(CartClear());
                  ref.read(cartProvider.notifier).clearCart();
                },
                child: const Text('clear cart'),
              ),
              Expanded(
                child: ListMenuWidget(items: cart),
              ),
            ],
          );
        }
      },
    );
  }
}
