import 'package:app_francesinha/public/features/home/domain/food_item.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'cart_provider.g.dart';

@riverpod
class Cart extends _$Cart {
  @override
  List<FoodItem> build() => <FoodItem>[];

  void addToCart(FoodItem item) {
    state = [...state, item];
  }

  void removeFromCart(FoodItem item) {
    state = state.where((element) => element != item).toList();
  }

  void clearCart() {
    state = [];
  }
}
