import 'package:app_francesinha/public/features/home/domain/food_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(CartState([])) {
    on<CartAddItem>((event, emit) {
      // State base
      final currentState = state;
      final items = currentState.items;
      // List items
      items.add(event.foodItem);

      // State
      emit(state.copyWith(items: items));
    });

    on<CartRemoveItem>((event, emit) {
      final currentState = state;
      final items = currentState.items;
      items.remove(event.foodItem);
      emit(state.copyWith(items: items));
    });

    on<CartClear>((event, emit) {
      emit(state.copyWith(items: []));
    });

    on<CartSetItemQuantity>((event, emit) {
      final currentState = state;
      final items = currentState.items;
      final index = items.indexOf(event.foodItem);
      final item = items[index];
      final newItem = item.copyWith(quantity: event.quantity);
      items[index] = newItem;
      emit(state.copyWith(items: items));
    });

    // on<CartEvent>((event, emit) {
    //   print(event);
    // });
  }
}
