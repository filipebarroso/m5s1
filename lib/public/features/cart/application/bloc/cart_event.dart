part of 'cart_bloc.dart';

@immutable
abstract class CartEvent {}

class CartAddItem extends CartEvent {
  CartAddItem(this.foodItem);

  final FoodItem foodItem;
}

class CartRemoveItem extends CartEvent {
  CartRemoveItem(this.foodItem);

  final FoodItem foodItem;
}

class CartSetItemQuantity extends CartEvent {
  CartSetItemQuantity(this.foodItem, this.quantity);

  final FoodItem foodItem;
  final int quantity;
}

class CartClear extends CartEvent {}
