part of 'cart_bloc.dart';

abstract class CartBaseState {}

class CartState extends CartBaseState {
  CartState(this.items);

  List<FoodItem> items;

  CartState copyWith({List<FoodItem>? items}) {
    return CartState(items ?? this.items);
  }
}
