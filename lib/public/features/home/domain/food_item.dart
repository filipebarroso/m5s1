import 'package:app_francesinha/public/features/detail/domain/francesinha.dart';

class FoodItem {
  const FoodItem({
    required this.image,
    required this.title,
    required this.subtitle,
    required this.price,
    this.quantity = 1,
  });

  final String image;
  final String title;
  final String subtitle;
  final String price;
  final int quantity;

  static FoodItem fromEntity(FoodItemEntity entity) {
    return FoodItem(
      image:
          'images/francesinha_${entity.id < 10 ? '0${entity.id}' : entity.id}.png',
      title: entity.name,
      subtitle: entity.description,
      price: '\$${entity.price.toString()}',
      quantity: 1,
    );
  }

  FoodItem copyWith({
    String? image,
    String? title,
    String? subtitle,
    String? price,
    int? quantity,
  }) {
    return FoodItem(
      image: image ?? this.image,
      title: title ?? this.title,
      subtitle: subtitle ?? this.subtitle,
      price: price ?? this.price,
      quantity: quantity ?? this.quantity,
    );
  }
}
