import 'package:app_francesinha/public/features/application/navigation_extensions.dart';
import 'package:app_francesinha/public/features/cart/application/bloc/cart_bloc.dart';
import 'package:app_francesinha/public/features/cart/application/riverpod/cart_provider.dart';
import 'package:app_francesinha/public/features/detail/application/selected_food_inherited_widget.dart';
import 'package:app_francesinha/public/features/home/domain/food_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ListMenuItemWidget extends ConsumerStatefulWidget {
  const ListMenuItemWidget({super.key, required this.foodItem});

  final FoodItem foodItem;

  @override
  ConsumerState createState() => ListMenuItemWidgetState();
}

class ListMenuItemWidgetState extends ConsumerState<ListMenuItemWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Card(
        color: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: InkWell(
          borderRadius: const BorderRadius.all(Radius.circular(20)),
          onTap: () {
            /// Save selected food to state
            final store = SelectedFoodInheritedWidget.of(context);
            final selected = store.selected;
            selected.setFood(widget.foodItem);

            // Navigate to detail page
            context.navigateToDetailPage();
          },
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: SizedBox(
                  height: 60,
                  width: 60,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(21)),
                    child: widget.foodItem.image.isNotEmpty
                        ? Image.asset(widget.foodItem.image)
                        : const Placeholder(),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.foodItem.title,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 8),
                    Text(
                      widget.foodItem.subtitle,
                      style: Theme.of(context).textTheme.titleMedium,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  Text(
                    widget.foodItem.price.toString(),
                    style: Theme.of(context).textTheme.displayMedium?.apply(
                          color: Theme.of(context).primaryColor,
                        ),
                  ),
                  BlocBuilder<CartBloc, CartState>(
                    builder: (context, state) {
                      return Checkbox(
                        // BLOC
                        // value: state.items.contains(widget.foodItem),
                        // Riverpod
                        value:
                            ref.watch(cartProvider).contains(widget.foodItem),
                        onChanged: (value) {
                          if (value == true) {
                            ref
                                .read(cartProvider.notifier)
                                .addToCart(widget.foodItem);
                          } else {
                            ref
                                .read(cartProvider.notifier)
                                .removeFromCart(widget.foodItem);
                          }
                          // _bloc(value, context);
                        },
                      );
                    },
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _bloc(bool? value, BuildContext context) {
    if (value == true) {
      context.read<CartBloc>().add(
            CartAddItem(widget.foodItem),
          );
    } else {
      context.read<CartBloc>().add(
            CartRemoveItem(widget.foodItem),
          );
    }
  }
}
