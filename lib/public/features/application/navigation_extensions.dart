import 'package:app_francesinha/public/features/application/navigations.dart';
import 'package:flutter/material.dart';

// All methods to help navigate the app
extension NavigationExtensions on BuildContext {
  void navigateHome() {
    Navigator.of(this).pushNamedAndRemoveUntil(
      AppNavigationPaths.home,
      (route) => false,
    );
  }

  void navigateToDetailPage() {
    Navigator.of(this).pushNamed(
      AppNavigationPaths.detail,
    );
  }

  void navigateAuthenticationPage() {
    Navigator.of(this).pushNamed(
      AppNavigationPaths.authentication,
    );
  }

  void navigateEmailLoginPage() {
    Navigator.of(this).pushNamed(
      AppNavigationPaths.emailLogin,
    );
  }

  void navigateEmailRegisterPage() {
    Navigator.of(this).pushNamed(
      AppNavigationPaths.emailRegister,
    );
  }
}
