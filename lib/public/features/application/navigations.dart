// All possible paths to navigate to.
class AppNavigationPaths {
  static const String home = '/';
  static const String detail = '/detail';
  static const String riverpod = '/riverpod';
  // Authentication
  static const String authentication = '/authentication';
  static const String emailLogin = '/email-login';
  static const String emailRegister = '/email-register';
}
