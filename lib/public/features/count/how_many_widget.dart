import 'package:app_francesinha/public/features/count/how_many_value_notifier.dart';
import 'package:flutter/material.dart';

typedef HowManyCounterCallback = Function(int value);

class HowManyCounter extends StatelessWidget {
  const HowManyCounter({
    super.key,
    required this.onHowManyCounter,
  });

  final HowManyCounterCallback onHowManyCounter;

  @override
  Widget build(BuildContext context) {
    final HowManyValueNotifier howManyValueNotifier = HowManyValueNotifier();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ValueListenableBuilder<int>(
        valueListenable: howManyValueNotifier,
        builder: (context, value, child) {
          return Row(
            children: [
              IconButton(
                onPressed: () {
                  howManyValueNotifier.decrement();
                  onHowManyCounter(value);
                },
                icon: const Icon(Icons.remove),
              ),
              Text(
                '$value',
                style: Theme.of(context).textTheme.displayMedium,
              ),
              IconButton(
                onPressed: () {
                  howManyValueNotifier.increment();
                  onHowManyCounter(value);
                },
                icon: const Icon(Icons.add),
              ),
              const SizedBox(width: 10, height: 10),
              IconButton(
                onPressed: () {
                  howManyValueNotifier.clear();
                  onHowManyCounter(value);
                },
                icon: const Icon(Icons.clear),
              ),
            ],
          );
        },
      ),
    );
  }
}
