import 'package:flutter/material.dart';

class HowManyValueNotifier extends ValueNotifier<int> {
  HowManyValueNotifier() : super(1);

  void increment() {
    value++;
    notifyListeners();
  }

  void decrement() {
    if (value > 1) {
      value--;
      notifyListeners();
    }
  }

  void clear() {
    value = 1;
    notifyListeners();
  }
}
