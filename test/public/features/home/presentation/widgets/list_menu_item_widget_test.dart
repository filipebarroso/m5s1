import 'package:app_francesinha/public/features/home/domain/food_item.dart';
import 'package:app_francesinha/public/features/home/presentation/widgets/list_menu_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../../test_utils.dart';

void main() {
  group(
    'ListMenuItemWidget',
    () {
      testWidgets(
        'Shows a widget Image when passing image url',
        (tester) async {
          final widget = wrapDirectionality(
            const ListMenuItemWidget(
              foodItem: FoodItem(
                title: '',
                subtitle: '',
                price: '',
                image: 'image/francesinha_01.png',
              ),
            ),
          );

          await tester.pumpWidget(widget);

          final result = find.byType(Image);

          expect(result, findsOneWidget);
        },
      );

      testWidgets(
        'Shows Placholder widget when imagePath not set',
        (tester) async {
          final widget = wrapDirectionality(
            const ListMenuItemWidget(
              foodItem: FoodItem(
                title: '',
                subtitle: '',
                price: '',
                image: '',
              ),
            ),
          );

          await tester.pumpWidget(widget);

          final result = find.byType(Placeholder);

          expect(result, findsOneWidget,
              reason: 'Shuld show a Placeholder widget');
        },
      );

      testWidgets(
        'Verify title text',
        (tester) async {
          const target = 'Francesinha 1';

          final widget = wrapDirectionality(
            const ListMenuItemWidget(
                foodItem: FoodItem(
              title: target,
              subtitle: '',
              price: '',
              image: '',
            )),
          );

          await tester.pumpWidget(widget);

          final result = find.byWidgetPredicate(
            (widget) {
              if (widget is Column) {
                if (widget.children.first is Text) {
                  final text = widget.children.first as Text;
                  return text.data == target;
                }
              }

              return widget is Center && widget.child is Text;
            },
          );

          expect(
            result,
            findsOneWidget,
          );
        },
      );

      testWidgets(
        'Verify title text',
        (tester) async {
          final widget = wrapDirectionality(
            const ListMenuItemWidget(
                foodItem: FoodItem(
              title: '',
              subtitle: 'Subtitle',
              price: '15',
              image: '',
            )),
          );

          await tester.pumpWidget(widget);

          expect(find.text('Subtitle'), findsOneWidget);
          expect(find.text('15'), findsOneWidget);
        },
      );
    },
  );
}
